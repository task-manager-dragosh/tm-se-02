package ru.dragosh.tm;

import java.util.List;
import java.util.Scanner;

public class Bootstrap {

    DataBase dataBase = new DataBase();

    public String scanWord() {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();

        return string;
    }

    public void helps() {
        System.out.println("Введите нужную команду:");
        for (String s : dataBase.commandsList) {
            System.out.println(s);
        }
    }

    public void addCollection(List<String> collection) {
        while (true) {
            String stringWhile = scanWord();
            add(collection, stringWhile);
            show(collection);
            if(" ".equals(stringWhile)) {
                break;
            }
        }
    }

    public void removeCollection(List<String> collection) {
        while (true) {
            String stringWhile = scanWord();
            remove(collection, stringWhile);
            break;
        }
    }

    public void add(List<String> arrayList, String name) {
        arrayList.add(name);
    }

    public void remove(List<String> arrayList, String name) {
        arrayList.remove(name);
    }

    public void show(List<String> arrayList) {
        for (String s : arrayList) {
            System.out.println(s);
        }
    }
}