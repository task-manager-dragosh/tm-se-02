package ru.dragosh.tm;

import java.util.ArrayList;
import java.util.List;

public class DataBase {

     List<String> commandsList = new ArrayList<String>(){{
        add("show all");
        add("add project (после завершения ввода названия/названий нажать Пробел + Ввод)");
        add("add task (после завершения ввода названия/названий нажать Пробел + Ввод)");
        add("remove project");
        add("remove task");
    }};

    List<String> projects = new ArrayList<String>(){{
        add("1");
        add("2");
        add("3");
    }};

    List<String> tasks = new ArrayList<String>(){{
        add("111");
        add("222");
        add("333");
    }};
}
