package ru.dragosh.tm;


public class Controller {

    Bootstrap bootstrap = new Bootstrap();
    DataBase dataBase = new DataBase();

    public void choiceCommand(String string) {
        switch(string) {
            case "help": {
                bootstrap.helps();
                break;
            }
            case "show all": {
                bootstrap.show(dataBase.projects);
                bootstrap.show(dataBase.tasks);
                break;
            }

            case "add project": {
                bootstrap.addCollection(dataBase.projects);
                break;
            }

            case "add task": {
                bootstrap.addCollection(dataBase.tasks);
                break;
            }

            case "remove project": {
                bootstrap.removeCollection(dataBase.projects);
                break;
            }

            case "remove task": {
                bootstrap.removeCollection(dataBase.tasks);
                break;
            }
        }
    }
}
